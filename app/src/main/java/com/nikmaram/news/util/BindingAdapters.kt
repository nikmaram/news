package com.nikmaram.news.util

import android.os.Build
import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.nikmaram.news.R

@BindingAdapter("imageUrl")
fun ImageView(view: ImageView,url:String?){
    Glide.with(view)
        .load(url)
        .centerCrop()
        .placeholder(R.drawable.placeholder)
        .error(R.drawable.placeholder)
        .into(view)
}

@BindingAdapter("isVisible")
fun View.isVisible(isShow:Boolean){
    if (isShow){
        this.visibility = View.VISIBLE
    }else{
        this.visibility = View.GONE
    }
}

@BindingAdapter("isVisInvis")
fun View.isVisInvis(isShow:Boolean){
    if (isShow){
        this.visibility = View.VISIBLE
    }else{
        this.visibility = View.INVISIBLE
    }
}

@BindingAdapter("htmltoTextView")
fun TextView.htmltoTextView(html:String){

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        text=Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
    } else {
        text=Html.fromHtml(html)
    }
}

