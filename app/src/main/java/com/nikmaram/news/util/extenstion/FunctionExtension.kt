@file:Suppress("NOTHING_TO_INLINE")

package com.nikmaram.news.util.extenstion


fun tryCatch(body: () -> Unit): Boolean {
    return try {
        body()
        true
    } catch (e: Throwable) {
        body.logE(e.message)
        false
    }
}