package com.nikmaram.news.util

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.nikmaram.news.databinding.ItemBinding
import com.nikmaram.news.model.Article

class NewsListAdapter : BaseAdapter(){


    enum class Type { LOADING, NORMAL }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            loading -> Type.LOADING.ordinal
            else -> Type.NORMAL.ordinal
        }
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):RecyclerView.ViewHolder{
        return when (viewType) {
            Type.LOADING.ordinal -> LoadingView.create(parent)
            else ->MyViewHolder(ItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false)
            )

        }

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(getItemViewType(position)){
            Type.NORMAL.ordinal -> {
                (holder as MyViewHolder).binding.data=getItem(position) as Article
                holder.binding.item.setOnClickListener {
                    onclickItem?.item(position)
                }
            }
            else -> {}
        }
    }


    class MyViewHolder(val binding:ItemBinding) : RecyclerView.ViewHolder(binding.root)



}
