package com.nikmaram.news.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}