package com.nikmaram.news.ui.MainFragment

import androidx.lifecycle.*
import com.nikmaram.news.model.NewsModel
import com.nikmaram.news.network.ApiRepository
import com.nikmaram.news.util.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.Dispatcher
import java.lang.Exception

class MainViewModel(val apiRepository: ApiRepository) :ViewModel(){

     var page = MutableLiveData<Int>(1)


    fun setPage() {
       page.value= page.value?.plus(1)
    }

   fun getLiveDataNews()=
       page.switchMap{ page ->
           liveData(Dispatchers.IO) {

               emit(Resource.loading(data = null))
               try{
                   emit(Resource.success(data = apiRepository.getNews(page)))
               }
               catch (e:Exception){
                   emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))

               }
           }

       }


}