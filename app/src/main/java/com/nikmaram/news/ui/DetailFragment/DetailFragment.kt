package com.nikmaram.news.ui.DetailFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nikmaram.news.databinding.FragmentDetailBinding
import com.nikmaram.news.util.NewsListAdapter

class DetailFragment : Fragment() {

    lateinit var binding:FragmentDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= FragmentDetailBinding.inflate(inflater,container,false)
        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.back.setOnClickListener {

            getActivity()?.onBackPressed()
        }
        val article=
            DetailFragmentArgs.fromBundle(
                requireArguments()
            ).Article
        binding.data = article
        binding.lifecycleOwner = viewLifecycleOwner


    }


}