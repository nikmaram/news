package com.nikmaram.news.ui.MainFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.nikmaram.news.databinding.FragmentNewsBinding
import com.nikmaram.news.model.Article
import com.nikmaram.news.model.NewsModel

import com.nikmaram.news.util.BaseAdapter
import com.nikmaram.news.util.NewsListAdapter
import com.nikmaram.news.util.Resource
import com.nikmaram.news.util.Status
import com.nikmaram.news.util.extenstion.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsFragment : Fragment() {
    private val mainViewModel: MainViewModel by viewModel()
    var binding: FragmentNewsBinding? = null

    private val newsListAdapter by lazy { NewsListAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (binding == null) {
            binding = FragmentNewsBinding.inflate(inflater, container, false)
            binding?.recyclerview?.apply {
                verticalLinearLayoutManager(requireActivity())
                adapter = newsListAdapter
            }
            binding?.recyclerview?.onLoadMore {
                newsListAdapter.showLoading()
                mainViewModel.setPage()
                getData()
            }
            getData()

        }
        return binding?.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        binding?.lyNotConnect?.btnConnection?.setOnClickListener {
            getData()
            binding?.lyNotConnect?.root?.gone()
            binding?.progress?.visible()
        }

    }

    private fun getData() {
         mainViewModel.getLiveDataNews().observe(viewLifecycleOwner){


                when (it.status) {
                    Status.LOADING -> {

                    }

                    Status.SUCCESS -> {
                        binding?.group?.visible()
                        binding?.lyNotConnect?.root?.gone()
                        binding?.progress?.gone()
                        it.data?.articles?.let { news -> newsListAdapter.addItemsRangeChange(news) }
                    }

                    Status.ERROR -> {
                        showError()
                    }
                }

                newsListAdapter.onClickItemListener(object : BaseAdapter.ONClickItem {
                    override fun item(id: Int) {
                        val article = newsListAdapter.getItem(id) as Article
                        findNavController().navigate(
                            NewsFragmentDirections.actionNewsFragmentToDetailFragment(
                                article
                            )
                        )

                    }

                })
            }


        }


        private fun showError() {
            newsListAdapter.hideLoading()

            binding?.group?.gone()
            binding?.lyNotConnect?.root?.visible()

        }

    }
