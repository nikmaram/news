package com.nikmaram.news.di

import com.nikmaram.news.network.ApiRepository
import com.nikmaram.news.ui.MainFragment.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule= module {
    single { provideLoggingInterceptor() }
    single { provideOkHttpClient(get()) }
    single { provideRetrofit(get()) }
    single { ApiRepository(get()) }
}

val viewModelsModule= module {

    viewModel {
        MainViewModel(get())
    }
}

