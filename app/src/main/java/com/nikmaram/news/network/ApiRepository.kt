package com.nikmaram.news.network

import com.nikmaram.news.model.NewsModel

class ApiRepository(val apiService: ApiService){


     suspend fun getNews(page:Int):NewsModel{

        return apiService.getNews(page)
    }

}