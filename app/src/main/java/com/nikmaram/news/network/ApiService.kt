package com.nikmaram.news.network

import com.nikmaram.news.model.NewsModel
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("everything?q=sport&apiKey=a428752d44cc4079a7c5983fbf09609c")
   suspend fun getNews(
        @Query("page") page:Int
    ):NewsModel


}