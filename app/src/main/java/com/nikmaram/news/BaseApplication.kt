package com.nikmaram.news

import android.app.Application
import com.nikmaram.news.di.applicationModule
import com.nikmaram.news.di.viewModelsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApplication :Application(){

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidContext(this@BaseApplication)
            modules(applicationModule,viewModelsModule)

        }
    }

}